package com.example.projetandroidfoot.ui.MatchsJour;

import static java.lang.String.valueOf;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projetandroidfoot.GetAPIkey.GetAPIKey;
import com.example.projetandroidfoot.R;
import com.example.projetandroidfoot.api.APIFootballMatch;
import com.example.projetandroidfoot.api.APIFootballMatchEvents;
import com.example.projetandroidfoot.model.events.getEvents;
import com.example.projetandroidfoot.model.match.GetMatchInfo;
import com.example.projetandroidfoot.model.match.MatchJourAdapter.MatchEventsAdapter;
import com.example.projetandroidfoot.model.match.MatchJourAdapter.MatchEventsData;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailMatchJourActivity  extends AppCompatActivity {
    private int id;
    private ViewPager2 viewPager;
    private DetailMatchJourCollectionAdapter adapter;

    public DetailMatchJourActivity() { }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_match_jour_activity);
        Intent intent=getIntent();
        this.id = Integer.parseInt(intent.getStringExtra("Id"));

        viewPager = findViewById(R.id.viewPagerDetailMatch);
        adapter = new DetailMatchJourCollectionAdapter(getSupportFragmentManager(),getLifecycle(), id);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = findViewById(R.id.tabLayoutMatchDetails);
        new TabLayoutMediator(tabLayout, viewPager,
                (tab, position) -> {
                    switch(position){
                        case 0:
                            tab.setText("Evénements");
                            break;
                        case 1:
                            tab.setText("Composition");
                            break;
                        case 2:
                            tab.setText("Statistiques");
                    }
                }).attach();


        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_match_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        callAPI2Match(""+this.id);

    }

    void callAPI2Match(String id){ // FONCTION REQUETE API
        Log.d("MYAPP", "API APPELEE");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api-football-beta.p.rapidapi.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIFootballMatch service = retrofit.create(APIFootballMatch.class);
        Random r = new Random(); //CLE ALEATOIRE
        String key= GetAPIKey.APIKey()[r.nextInt(7)];
        service.listRepos(getString(R.string.apihost),key,"fixtures",id).enqueue(new Callback<GetMatchInfo>(){
            @Override
            public void onResponse(Call<GetMatchInfo> call, Response<GetMatchInfo> response) {
                GetMatchInfo rep = response.body();
                if (rep != null) {
                    Log.d("RESPONSE API2", rep.toString());

                    setTitle(rep.getLeagueName());

                    TextView tAway = findViewById(R.id.awayteamtext); //SET AWAY TEAM
                    tAway.setText(rep.getAwayTeamName());
                    TextView tHome = findViewById(R.id.hometeamtext); //SET HOME TEAM
                    tHome.setText(rep.getHomeTeamName());
                    Picasso.get().load(rep.getHomeTeamLogo()).into((ImageView) findViewById(R.id.imagehometeam));//SET HOME TEAM LOGO
                    Picasso.get().load(rep.getAwayTeamLogo()).into((ImageView) findViewById(R.id.imageawayteam));//SET AWAY TEAM LOGO
                    TextView tStade = findViewById(R.id.textStade); //SET NOM STADE
                    tStade.setText(rep.getVenue());
                    TextView tScore = findViewById(R.id.matchscore); //SET SCORE
                    tScore.setText(rep.getScore());
                    TextView tDate = findViewById(R.id.datetext); //SET DATE
                    String[] separated = rep.getDate().split("T");
                    TextView tChampionnat= findViewById(R.id.button); //SET NOM CHAMPIONNAT
                    tChampionnat.setText(rep.getLeagueName());
                    String date = separated[0];
                    String heure = separated[1];
                    String[] dateSeparated = date.split("-");
                    String[] heureSeparated = heure.split(":");
                    int heureFuseauFrance = Integer.parseInt(heureSeparated[0])+2; //METTRE A NIVEAU VERS FUSEAU HORAIRE PARIS
                    tDate.setText(dateSeparated[2]+"."+dateSeparated[1]+"."+dateSeparated[0]+" " + heureFuseauFrance+":"+heureSeparated[1]);
                    TextView tMinute = findViewById(R.id.minutetext); //SET MINUTE
                    if(rep.getMatchStatus().equals("FT") == true) { //AJOUTER + TARD DINSTINCTION MATCH FINI ET A VENIR
                        tMinute.setText("Terminé");
                        tScore.setTextColor(Color.WHITE);
                        tMinute.setTextColor(Color.WHITE);
                    }
                    else if(rep.getMatchStatus().equals("HT") == true){
                        tMinute.setText("Mi-Temps ");}
                    else if(rep.getMatchStatus().equals("1H") == true){
                        tMinute.setText("1.MT "+rep.getMatchMinute());}
                    else if(rep.getMatchStatus().equals("2H") == true){
                        tMinute.setText("2.MT "+rep.getMatchMinute());}
                    else {
                        tScore.setTextColor(Color.WHITE);
                        tMinute.setTextColor(Color.WHITE);
                        tMinute.setText("A venir");}

                }

            }
            @Override
            public void onFailure(Call<GetMatchInfo> call, Throwable t) {
                Log.e("err",t.toString());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        this.finish();
        return super.onOptionsItemSelected(item);
    }

}