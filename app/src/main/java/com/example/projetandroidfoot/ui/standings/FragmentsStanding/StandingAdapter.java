package com.example.projetandroidfoot.ui.standings.FragmentsStanding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.projetandroidfoot.R;
import com.squareup.picasso.Picasso;


public class StandingAdapter extends RecyclerView.Adapter<StandingAdapter.ViewHolder>{
    private StandingData[] listdata;
    private int size = 0;

    // RecyclerView recyclerView;
    public StandingAdapter(StandingData[] listdata) {
        if (listdata==null){
            this.size=0;
        }else{
            this.size=listdata.length;
        }
        this.listdata = listdata;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final StandingData standingData = listdata[position];
        holder.textView.setText(listdata[position].getPositionStanding());
        holder.textPointsClassement.setText(listdata[position].getPointsStanding());
        holder.textTeamClassement.setText(listdata[position].getTeamStanding());
        holder.textButClassement.setText(listdata[position].getgoalDiffStanding());
        Picasso.get().load(listdata[position].getImgId()).into(holder.imageView);
        //holder.imageView.setImageResource(listdata[position].getImgId());
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(),"click on item: "+ standingData.getPositionStanding(),Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return this.size;
    }

    public void addAll(StandingData[] data){
        this.listdata=new StandingData[data.length];
        for(int i=0;i<data.length;i++){
            this.listdata[i]=data[i];
        }
        this.size=data.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;
        public TextView textTeamClassement;
        public TextView textButClassement;
        public TextView textPointsClassement;
        public RelativeLayout relativeLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.eventImage);
            this.textView = (TextView) itemView.findViewById(R.id.textView);
            this.textPointsClassement = (TextView) itemView.findViewById(R.id.scoreHome);
            this.textTeamClassement = (TextView) itemView.findViewById(R.id.textEvent);
            this.textButClassement = (TextView) itemView.findViewById(R.id.textButClassement);
            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.relativeLayout);
        }
    }
}