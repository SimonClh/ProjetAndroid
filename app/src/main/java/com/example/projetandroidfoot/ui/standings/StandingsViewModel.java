package com.example.projetandroidfoot.ui.standings;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class StandingsViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    public StandingsViewModel() {
            mText = new MutableLiveData<>();
            String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
            mText.setValue(new String(currentDate));
        }

        public LiveData<String> getText() {
            return mText;
        }
    }
