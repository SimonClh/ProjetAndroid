package com.example.projetandroidfoot.ui.MatchsJour.MatchJourFragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projetandroidfoot.GetAPIkey.GetAPIKey;
import com.example.projetandroidfoot.R;
import com.example.projetandroidfoot.api.APIFootballMatchEvents;
import com.example.projetandroidfoot.model.events.getEvents;
import com.example.projetandroidfoot.model.match.MatchJourAdapter.MatchEventsAdapter;
import com.example.projetandroidfoot.model.match.MatchJourAdapter.MatchEventsData;

import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailEvenementsFragment extends Fragment {

    private int id=0;
    View view;
    RecyclerView recyclerView;
    MatchEventsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.detail_evenements_fragment, container, false);

        Bundle args = getArguments();
        this.id=args.getInt("Id");

        recyclerView = view.findViewById(R.id.recyclerEvenementsMatch);
        adapter = new MatchEventsAdapter(null);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext(),LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(adapter);

        callAPI2MatchEvent(""+this.id);//APPELER API AFFICHER MATCH EVENTS
        return view;

    }


    void callAPI2MatchEvent(String id){ // FONCTION REQUETE API
        Log.d("MYAPP", "Fragment evenements api reponse");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api-football-beta.p.rapidapi.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIFootballMatchEvents service = retrofit.create(APIFootballMatchEvents.class);
        Random r = new Random(); //CLE ALEATOIRE
        String key= GetAPIKey.APIKey()[r.nextInt(7)];
        service.listRepos(getString(R.string.apihost),key,id).enqueue(new Callback<getEvents>(){
            @Override
            public void onResponse(Call<getEvents> call, Response<getEvents> response) {
                getEvents rep = response.body();
                if (rep != null) {

                    List<getEvents.EventMatch> retourMatchlist = rep.getEvents();

                    MatchEventsData[] listEvents = new MatchEventsData[retourMatchlist.size()];
                    for(int i=0;i<retourMatchlist.size();i++){
                        getEvents.EventMatch event = retourMatchlist.get(i);
                        listEvents[i] = new MatchEventsData(event.getType(),event.time.getTime(),event.player.getScorer(),event.assist.getAssist(), event.getDetail());
                    }

                    adapter.addAll(listEvents);
                    adapter.notifyDataSetChanged();

                }

            }
            @Override
            public void onFailure(Call<getEvents> call, Throwable t) {
                Log.e("err",t.toString());
            }
        });
    }

}
