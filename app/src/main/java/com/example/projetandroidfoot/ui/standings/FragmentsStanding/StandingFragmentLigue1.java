package com.example.projetandroidfoot.ui.standings.FragmentsStanding;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projetandroidfoot.GetAPIkey.GetAPIKey;
import com.example.projetandroidfoot.R;
import com.example.projetandroidfoot.api.APIFootballStandings;
import com.example.projetandroidfoot.databinding.FragmentStandingBinding;
import com.example.projetandroidfoot.model.standings.Competition;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StandingFragmentLigue1 extends Fragment {

    FragmentStandingBinding binding;
    RecyclerView recyclerView;
    StandingAdapter adapter;

    void callnewAPIStandings(String idChampionnat, int season, String key){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api-football-beta.p.rapidapi.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIFootballStandings service = retrofit.create(APIFootballStandings.class);
        service.listRepos(getString(R.string.apihost), key,season,idChampionnat).enqueue(new Callback<Competition>() {

            @Override
            public void onResponse(Call<Competition> call, Response<Competition> response) {
                Competition rep = response.body();
                int yearEnd= season+1;

                if (rep != null) {
                    Log.d("RESPONSE API", rep.getLeagueName()+" Saison "+ season+""+yearEnd);
                    TextView tLeagueName = getView().findViewById(R.id.competitionNameText); //SET LEAGUE NAME
                    tLeagueName.setText(rep.getLeagueName());
                    TextView tSeasonName = getView().findViewById(R.id.textSeason); //SET LEAGUE SEASON
                    tSeasonName.setText(rep.getLeagueSeason());
                    Picasso.get().load(rep.getLeagueLogo()).into((ImageView) getView().findViewById(R.id.LeagueLogo2));//SET LEAGUE LOGO
                    List<List<Competition.Leagues.LeaguesInfo.StandingsObject>> retourClassement = rep.getLeagueStandings();


                    StandingData[] standingData = new StandingData[retourClassement.get(0).size()];

                    for(int i=0;i<retourClassement.get(0).size();i++){
                        Competition.Leagues.LeaguesInfo.StandingsObject team = retourClassement.get(0).get(i);
                        Log.d("data", team.getName());
                        standingData[i] = new StandingData(team.getRank(), team.getName(),team.getButs(),team.getPoints(), team.getLogo());
                    }


                    adapter.addAll(standingData);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<Competition> call, Throwable t) {
                Log.e("err", t.toString());
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View drawer = inflater.inflate(R.layout.fragment_standing,container, false);
        // Inflate the layout for this fragment
        binding = FragmentStandingBinding.inflate(inflater, container, false);

        recyclerView = drawer.findViewById(R.id.recyclerViewLigue1);
        adapter = new StandingAdapter(null);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this.getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);

        Random r = new Random(); //CLE ALEATOIRE
        String key= GetAPIKey.APIKey()[r.nextInt(7)];

        callnewAPIStandings("61",2021,key);
        return drawer;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

}