package com.example.projetandroidfoot.ui.standings;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.projetandroidfoot.ui.standings.FragmentsStanding.StandingFragmentBundesliga;
import com.example.projetandroidfoot.ui.standings.FragmentsStanding.StandingFragmentLigue1;
import com.example.projetandroidfoot.ui.standings.FragmentsStanding.StandingFragmentPremierLeague;

public class StandingsCollectionAdapter extends FragmentStateAdapter {
    public StandingsCollectionAdapter(Fragment fragment){
        super(fragment);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        //par defaut
        Fragment fragment = new StandingFragmentLigue1();
        switch(position){
            case 0:
                fragment = new StandingFragmentLigue1();
                break;
            case 1:
                fragment = new StandingFragmentPremierLeague();
                break;
            case 2:
                fragment = new StandingFragmentBundesliga();
                break;
        }
        return fragment;
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}