package com.example.projetandroidfoot.ui.favoris;
import static java.lang.String.valueOf;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projetandroidfoot.GetAPIkey.GetAPIKey;
import com.example.projetandroidfoot.R;
import com.example.projetandroidfoot.api.APIFootballListMatch;
import com.example.projetandroidfoot.databinding.FragmentFavorisBinding;
import com.example.projetandroidfoot.databinding.FragmentHomeBinding;
import com.example.projetandroidfoot.model.match.GetMatchInfo;
import com.example.projetandroidfoot.model.match.MatchJourAdapter.MatchJourAdapter;
import com.example.projetandroidfoot.model.match.MatchJourAdapter.MatchJourData;
import com.example.projetandroidfoot.ui.MatchsJour.DetailMatchJourActivity;
import com.example.projetandroidfoot.ui.favoris.favorisAdapter.FavorisAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FavorisFragment extends Fragment implements MatchJourAdapter.OnNoteListener{

    private FragmentFavorisBinding binding;
    private MatchJourAdapter adapter;
    List<GetMatchInfo.OpenMatch> retourMatchlist;
    private RecyclerView recyclerView;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentFavorisBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        TextView nomatch = (TextView) root.findViewById(R.id.noMatch);
        nomatch.setVisibility(View.INVISIBLE);

        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerFavoris);
        this.adapter = new MatchJourAdapter(null,this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        String currentDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        callAPIListMatch("113","2021",currentDate);
        return root;
    }
    void callAPIListMatch(String idleague,String season,String date){ // FONCTION REQUETE API
        Log.d("MYAPP", "API APPELEE");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api-football-beta.p.rapidapi.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIFootballListMatch service = retrofit.create(APIFootballListMatch.class);
        Random r = new Random(); //CLE ALEATOIRE
        String key= GetAPIKey.APIKey()[r.nextInt(7)];
        service.listRepos(getString(R.string.apihost),key,idleague,season,date).enqueue(new Callback<GetMatchInfo>(){
            @Override
            public void onResponse(Call<GetMatchInfo> call, Response<GetMatchInfo> response) {
                GetMatchInfo rep = response.body();
                if (rep != null) {
                    Log.d("RESPONSE LIST MATCH", rep.toString());
                    if(rep.results.equals("0")) {
                        TextView nomatch = (TextView) getView().findViewById(R.id.noMatch);
                        nomatch.setVisibility(View.VISIBLE);
                    }
                    else{
                        GetMatchInfo.OpenMatch.MatchDetailsTeams repMatch = rep.getMatchTeams();
                        retourMatchlist = rep.getMatchList();
                        MatchJourData[] myListData = new MatchJourData[retourMatchlist.size()];
                        for (int i = 0; i < retourMatchlist.size(); i++) {
                            GetMatchInfo.OpenMatch team = retourMatchlist.get(i);
                            myListData[i] = new MatchJourData(team.getHomeTeamName(), team.getAwayTeamName(), team.getHomeTeamLogo(), team.getAwayTeamLogo(), team.getHomeScore(), team.getAwayScore(), team.getMatchStatus(), team.getLiveStatus());
                        }
                        adapter.addAll(myListData);
                        adapter.notifyDataSetChanged();
                    }
                }
            }
            @Override
            public void onFailure (Call < GetMatchInfo > call, Throwable t){
                Log.e("err", t.toString());
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
    @Override
    public void onNoteClick(int adapterPosition) {
        View view = getView();
        Intent intent = new Intent(view.getContext(), DetailMatchJourActivity.class);
        intent.putExtra("Id", retourMatchlist.get(adapterPosition).getMatchId());
        view.getContext().startActivity(intent);

    }
}