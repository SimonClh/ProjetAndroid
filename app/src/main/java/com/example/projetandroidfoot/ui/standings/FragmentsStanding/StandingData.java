package com.example.projetandroidfoot.ui.standings.FragmentsStanding;

public class StandingData {
    private String positionStanding;
    private String pointsStanding;
    private String teamStanding;
    private String imgId;
    private String goalDiffStanding;

    public StandingData(String positionStanding, String teamStanding, String goalDiffStanding, String pointsStanding, String imgId) {
        this.positionStanding = positionStanding;
        this.pointsStanding = pointsStanding;
        this.teamStanding = teamStanding;
        this.goalDiffStanding= goalDiffStanding;
        this.imgId = imgId;
    }


    public String getPositionStanding() {
        return positionStanding;
    }
    public String getImgId() {
        return imgId;
    }
    public String getPointsStanding() {
        return pointsStanding;
    }
    public String getTeamStanding() {
        return teamStanding;
    }
    public String getgoalDiffStanding() {
        return goalDiffStanding;
    }
}