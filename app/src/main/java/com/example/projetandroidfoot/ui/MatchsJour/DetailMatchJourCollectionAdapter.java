package com.example.projetandroidfoot.ui.MatchsJour;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.projetandroidfoot.ui.MatchsJour.MatchJourFragments.DetailEvenementsFragment;
import com.example.projetandroidfoot.ui.MatchsJour.MatchJourFragments.DetailStatsFragment;
import com.example.projetandroidfoot.ui.MatchsJour.MatchJourFragments.DetailTeamFragment;

public class DetailMatchJourCollectionAdapter extends FragmentStateAdapter {

    private int id=0;

    public DetailMatchJourCollectionAdapter(FragmentManager f, Lifecycle lifecycle, int id){
        super(f,lifecycle);
        this.id=id;
    }


    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment fragment = new DetailEvenementsFragment();
        switch (position){
            case 0:
                fragment = new DetailEvenementsFragment();
                break;
            case 1:
                fragment= new DetailTeamFragment();
                break;
            case 2:
                fragment = new DetailStatsFragment();
        }
        Bundle args = new Bundle();
        args.putInt("Id", this.id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
