package com.example.projetandroidfoot.ui.favoris;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class FavorisViewModel extends ViewModel {

    private MutableLiveData<String> mText;


    public FavorisViewModel() {
        mText = new MutableLiveData<>();
    }

    public LiveData<String> getText() {
        return mText;
    }
}