package com.example.projetandroidfoot.ui.standings;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;

import com.example.projetandroidfoot.R;
import com.example.projetandroidfoot.databinding.FragmentStandingshomeBinding;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class StandingsFragment extends Fragment {

    private StandingsViewModel standingsViewModel;
    private FragmentStandingshomeBinding binding;
    private ViewPager2 viewPager;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        standingsViewModel = new ViewModelProvider(this).get(StandingsViewModel.class);
        binding = FragmentStandingshomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        StandingsCollectionAdapter adapter = new StandingsCollectionAdapter(this);
        viewPager = view.findViewById(R.id.viewPager);
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = view.findViewById(R.id.tabLayout);
        new TabLayoutMediator(tabLayout, viewPager,
                (tab, position) -> {
                    switch(position){
                        case 0:
                            tab.setText("Ligue 1");
                            break;
                        case 1:
                            tab.setText("Premier League");
                            break;
                        case 2:
                            tab.setText("Bundesliga");
                    }
                }).attach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}