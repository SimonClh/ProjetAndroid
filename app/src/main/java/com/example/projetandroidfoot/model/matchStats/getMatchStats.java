package com.example.projetandroidfoot.model.matchStats;

import androidx.annotation.NonNull;

import java.util.List;

public class getMatchStats {

    private String get;
    private int results;
    private List<MatchStats> response;

    public class MatchStats {

        public Team team;
        private List<Statistics> statistics;

        public class Team {
            private String name;
            private String logo;
            @NonNull
            @Override
            public String toString() {
                return "";
            }
            public String getName() {
                return name;
            }
            public String getLogo() {
                return logo;
            }
            public Team(){}

        }

        public class Statistics {

            private String stat;
            private String number;

            @NonNull
            @Override
            public String toString() {
                return "";
            }
            public String getStatName() {
                return stat;
            }
            public String getNumber() {
                return number;
            }

            public Statistics(){}
        }

        public MatchStats(){}
    }

    @NonNull
    @Override
    public String toString() {
        return get+response.get(0).toString();
    }
    public String getTeam() {
        return response.get(0).team.getName();
    }
    public String getLogo() {
        return response.get(0).team.getLogo();
    }
    public String getPlayerOnName() {
        return response.get(0).statistics.get(0).getStatName();
    }
    public String getPlayerOnNumber() {
        return response.get(0).statistics.get(0).getNumber();
    }
    public List<MatchStats> getMatchStats() {
        return response;
    }
    public getMatchStats(){}

}
