package com.example.projetandroidfoot.model.standings;

import androidx.annotation.NonNull;

import com.example.projetandroidfoot.model.match.GetMatchInfo;

import java.util.List;

public class Competition {

    private String results;
    private List<Leagues> response;

    public class Leagues {

        private LeaguesInfo league;

        public class LeaguesInfo {

            private String name;
            private String country;
            private String logo;
            private int season;
            public List<List<StandingsObject>> standings;

            public class StandingsObject {
                private String rank;
                private int points;
                private int goalsDiff;
                private int season;
                private StandingsTeam team;

                public String getRank() {
                    return this.rank;
                }
                public String getName() {
                    return this.team.name;
                }
                public String getLogo() {
                    return this.team.logo;
                }
                public String getButs() {
                    return ""+ this.goalsDiff;
                }
                public String getPoints() {
                    return "" + this.points;
                }

                public class StandingsTeam {
                    private String name;
                    private String logo;
                    public String toString() {
                        return this.name;
                    }
                }

                public String toString() {
                    return "Pos:"+rank+"  "+team+" points:"+ points+" DiffGoal"+ goalsDiff;
                }
            }

            public String makeSeasonString() {
                int sp=season+1;
                return "Saison "+season+"/"+sp;

            }

        }
    }

    public String getLeagueName() {
        return response.get(0).league.name;
    }
    public String getLeagueCountry() {
        return response.get(0).league.country;
    }
    public String getLeagueLogo() {
        return response.get(0).league.logo;
    }
    public String getLeagueSeason() {
        return response.get(0).league.makeSeasonString();
    }
    public List<List<Leagues.LeaguesInfo.StandingsObject>> getLeagueStandings() {
        return response.get(0).league.standings;
    }

}
