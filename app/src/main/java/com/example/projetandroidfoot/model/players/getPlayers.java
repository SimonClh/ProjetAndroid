package com.example.projetandroidfoot.model.players;

import androidx.annotation.NonNull;

import java.util.List;

public class getPlayers {

    private String get;
    private int results;
    private List<TeamLineUp> response;

    public class TeamLineUp {

        public Team team;
        public Coach coach;
        private String formation;
        private List<StartXI> startXI;
        private List<Substitutes> substitutes;

        public String getFormation(){return formation; }

        public class Team {
            private String name;
            private String logo;
            @NonNull
            @Override
            public String toString() {
                return "";
            }
            public String getName() {
                return name;
            }
            public String getLogo() {
                return logo;
            }
            public Team(){}

        }

        public class Coach {
            private String name;
            @NonNull
            @Override
            public String toString() {
                return "";
            }
            public String getCoachName() {
                return name;
            }
            public Coach(){}
        }

        public class StartXI {

            public PlayerOn playerOn;

            public class PlayerOn {

                private String name;
                private String number;
                private String pos;
                @NonNull
                @Override
                public String toString() {
                    return "";
                }
                public String getPlayerOnName() {
                    return name;
                }
                public String getNumber() {
                    return number;
                }
                public String getPos() {
                    return pos;
                }

                public PlayerOn(){};
            }

        }

        public class Substitutes {

            public Sub sub;

            public class Sub {

                private String name;
                private String number;
                private String pos;
                @NonNull
                @Override
                public String toString() {
                    return "";
                }
                public String getSubstituteName() {
                    return name;
                }
                public String getNumber() {
                    return number;
                }
                public String getPos() {
                    return pos;
                }

                public Sub(){};
            }

        }

        public TeamLineUp(){}
    }

    @NonNull
    @Override
    public String toString() {
        return get+response.get(0).toString();
    }
    public String getTeam() {
        return response.get(0).team.getName();
    }
    public String getLogo() {
        return response.get(0).team.getLogo();
    }
    public String getCoach() {
        return response.get(0).coach.getCoachName();
    }
    public String getPlayerOnName() {
        return response.get(0).startXI.get(0).playerOn.getPlayerOnName();
    }
    public String getPlayerOnNumber() {
        return response.get(0).startXI.get(0).playerOn.getNumber();
    }
    public String getPlayerOnPos() {
        return response.get(0).startXI.get(0).playerOn.getPos();
    };
    public String getSubstituteName() {
        return response.get(0).substitutes.get(0).sub.getSubstituteName();
    }
    public String getSubstituteNumber() {
        return response.get(0).substitutes.get(0).sub.getNumber();
    }
    public String getSubstitutePos() {
        return response.get(0).substitutes.get(0).sub.getPos();
    };
    public List<TeamLineUp> getPlayers() {
        return response;
    }
    public getPlayers(){}

}
