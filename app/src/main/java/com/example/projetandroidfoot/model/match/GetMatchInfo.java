package com.example.projetandroidfoot.model.match;

import android.graphics.Color;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetMatchInfo {
    private String get;
    public String results;
    private List<OpenMatch> response;
    public class OpenMatch {
        private MatchDetails fixture;
        private Goals goals;
        private League league;
        private MatchDetailsTeams teams;
        public class MatchDetailsTeams {
            private TeamDetails home;
            private TeamDetails away;
            public class TeamDetails {
                private String name;
                private String logo;
                @NonNull
                @Override
                public String toString() {
                    return name+logo;
                }
                public String getName() {
                    return name;
                }
                public String getLogo() {
                    return logo;
                }
                public TeamDetails(){}

            }
            @NonNull
            @Override
            public String toString() {
                return ""+home+away;
            }
            public MatchDetailsTeams(){}

        }
        public class MatchDetails {
            private String id;
            private String referee;
            private String date;
            private VenueDetails venue;
            private StatusMatch status;
            public class StatusMatch {

                @SerializedName("short")
                private String shortString;
                private int elapsed;
                @NonNull
                @Override

                public String toString() {
                    return referee+date+venue;
                }
                public String getMatchID() {
                    return id;
                }
                public StatusMatch(){}

            }
            @NonNull
            @Override
            public String toString() {
                return referee+date+venue;
            }
            public String getVenue() {
                return ""+venue;
            }
            public MatchDetails(){}

        }
        public class Goals {
            private int home;
            private int away;
            @NonNull
            @Override
            public String toString() {
                return "";
            }
            public String getScore() {
                return home+"-"+away;
            }
            public String getHomeScore() {
                    return ""+home;
            }
            public String getAwayScore() {
                return ""+away;
            }
            public Goals(){}

        }
        public class League {
            private String name;
            @NonNull
            @Override
            public String toString() {
                return "";
            }
            public League(){}

        }
        public class VenueDetails {
            private String name;
            @NonNull
            @Override
            public String toString() {
                return name;
            }
            public VenueDetails(){}

        }
        @NonNull
        @Override
        public String toString() {
            return "" + teams;
        }
        public String getMatchId(){return fixture.id;}
        public String getHomeTeamName() {
            return teams.home.getName();
        }
        public String getHomeTeamLogo() {
            return teams.home.getLogo();
        }
        public String getAwayTeamLogo() {
            return teams.away.getLogo();
        }
        public String getAwayTeamName() {
            return teams.away.getName();
        }
        public String getHomeScore() {
            return goals.getHomeScore();
        }
        public String getAwayScore() {
            return goals.getAwayScore();
        }
        public String getMatchStatus() {
            String ms;
            if (fixture.status.shortString.equals("FT") == true) {
                ms = "Terminé";
            }
            else if (fixture.status.shortString.equals("CANC") == true) {
                ms = "Annulé";
            }
            else if (fixture.status.shortString.equals("PST") == true) {
                ms = "Reporté";
            }else if (fixture.status.shortString.equals("1H") == true) {
                ms = "1.MT " + fixture.status.elapsed+"'";
            } else if (fixture.status.shortString.equals("2H") == true) {
                ms = "2.MT " + fixture.status.elapsed+"'";
            }
            else if (fixture.status.shortString.equals("HT") == true) {
                ms = "Mi-Temps";
            }
            else if (fixture.status.shortString.equals("NS") == true) {
                ms = "A venir";
            }
            else if (fixture.status.shortString.equals("AET") == true) {
                ms = "Après prolongation";
            }
            else if (fixture.status.shortString.equals("ET") == true) {
                ms = "Prolongation " + fixture.status.elapsed+"'";
            }
            else if (fixture.status.shortString.equals("PEN") == true) {
                ms = "Tirs aux buts ";
            }
            else {
                ms = fixture.status.shortString;
            }
            return "" + ms;
        }
        public int getLiveStatus() {
            int tmp =0;
            if (fixture.status.shortString.equals("FT") == true) {
                tmp=0;
            }
            else if (fixture.status.shortString.equals("PST") == true) {
                tmp=0;
            }
            else if (fixture.status.shortString.equals("CANC") == true) {
                tmp=0;
            }
            else if (fixture.status.shortString.equals("1H") == true) {
                tmp=1;
            } else if (fixture.status.shortString.equals("2H") == true) {
                tmp=1;
            }
            else if (fixture.status.shortString.equals("HT") == true) {
                tmp=1;
            }
            else if (fixture.status.shortString.equals("ET") == true) {
                tmp=1;
            }
            else if (fixture.status.shortString.equals("PEN") == true) {
                tmp=1;
            }
            else{
                tmp=0;
            }
            return tmp;
        }
        public OpenMatch(){}

    }

    @NonNull
    @Override

    public String toString() {
            return response.toString();
        }
    public List<OpenMatch> getMatchList() {
        return response;
    }
    public String getHomeTeamName() {
        return response.get(0).getHomeTeamName();
    }
    public String getHomeTeamLogo() {
        return response.get(0).getHomeTeamLogo();
    }
    public String getAwayTeamLogo() {
        return response.get(0).getAwayTeamLogo();
    }
    public String getAwayTeamName() {
        return response.get(0).getAwayTeamName();
    }
    public String getVenue() {
        return response.get(0).fixture.getVenue();
    }
    public String getScore() {
        return response.get(0).goals.getScore();
    }
    public String getDate() {
        return response.get(0).fixture.date;
    }
    public String getLeagueName() {
        return response.get(0).league.name;
    }
    public String getMatchStatus() {
        return response.get(0).fixture.status.shortString;
    }
    public String getMatchMinute() {
        return ""+ response.get(0).fixture.status.elapsed+"'";
    }
    public OpenMatch.MatchDetailsTeams getMatchTeams(){return response.get(0).teams;}
    public GetMatchInfo(){}

}
