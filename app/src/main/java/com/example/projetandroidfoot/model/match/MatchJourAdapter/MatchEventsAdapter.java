package com.example.projetandroidfoot.model.match.MatchJourAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projetandroidfoot.R;

public class MatchEventsAdapter extends RecyclerView.Adapter<MatchEventsAdapter.ViewHolder> {

    private MatchEventsData[] data;
    private int size=0;

    public MatchEventsAdapter(MatchEventsData[] data){
        if(data==null){
            this.size=0;
        }else{
            this.size=data.length;
        }
        this.data=data;
    }

    public void addAll(MatchEventsData[] data){
        this.data=new MatchEventsData[data.length];
        for(int i=0;i<data.length;i++){
            this.data[i]=data[i];
        }
        this.size=data.length;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.matchevent_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final MatchEventsData dataMatch = data[position];
        holder.EventName.setText(data[position].getPlayerString()+data[position].getAssistString());
        holder.MinuteString.setText(data[position].getMinuteString());
        if(data[position].getEventName().equals("Goal")) {
            holder.EventImage.setImageResource(R.drawable.ic_baseline_sports_soccer_24);
        }
        else if(data[position].getEventName().equals("subst")) {
            holder.EventImage.setImageResource(R.drawable.substitution);
        }
        else if(data[position].getEventName().equals("Card")) {
            if(data[position].getEventDetail().equals("Yellow Card")){
            holder.EventImage.setImageResource(R.drawable.ic_yellow_card);}
            else{
                holder.EventImage.setImageResource(R.drawable.ic_red_card);}
            }
        else{
            holder.EventImage.setImageResource(android.R.drawable.ic_dialog_alert);
        }
    }


    @Override
    public int getItemCount() {
        return this.size;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView EventName;
        public TextView MinuteString;
        public ImageView EventImage;
        public ViewHolder(View itemView) {
            super(itemView);
            this.EventName = (TextView) itemView.findViewById(R.id.textEvent);
            this.MinuteString = (TextView) itemView.findViewById(R.id.textEventMinute);
            this.EventImage = (ImageView) itemView.findViewById(R.id.eventImage);
        }
    }

}
