package com.example.projetandroidfoot.model.events;

import androidx.annotation.NonNull;

import com.example.projetandroidfoot.model.standings.Competition;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class getEvents {

    private String get;
    private int results;
    private List<EventMatch> response;

    public class EventMatch {

        //private MatchDetails fixture;
        public Time time;
        public Player player;
        private MatchDetailsTeam teams;
        public Assist assist;
        private String type;
        private String detail;

        public String getType(){return type; }
        public String getDetail(){return detail;}

        public class Time {
            private int elapsed;
            @NonNull
            @Override
            public String toString() {
                return ""+elapsed;
            }
            public String getTime() {
                return elapsed + "'";
            }
            public Time(){}

        }

        public class MatchDetailsTeam {
            private String teamName;
            @NonNull
            @Override
            public String toString() {
                return "";
            }
            public String getTeamName() {
                return teamName;
            }
            public MatchDetailsTeam(){}
        }

        public class Player {
            private String name;
            @NonNull
            @Override
            public String toString() {
                return ""+name;
            }
            public String getScorer() { return ""+name;}
            public Player(){}

        }

        public class Assist {
            private String name;
            @NonNull
            @Override
            public String toString() {
                return "";
            }
            public String getAssist() {
                if (name == null){
                    return"";
                }
            else{
                    return " ("+name+")";
                }
            }
            public Assist(){}

        }
        public String toString() {
            return time+getType()+detail;
        }
        public EventMatch(){}
    }

    @NonNull
    @Override
    public String toString() {
        return get+response.get(0).toString();
    }
    public String getType() {
        return response.get(0).getType();
    }
    public String getDetail() {
        return response.get(0).detail;
    }
    public String getTime() {
        return response.get(0).time.getTime();
    }
    public String getTeamName() {
        return response.get(0).teams.getTeamName();
    }
    public String getPlayer() {
        return response.get(0).player.getScorer();
    }
    public String getAssist() {
        return response.get(0).assist.getAssist();
    }
    public List<EventMatch> getEvents() {
        return response;
    }
    public getEvents(){}

}
