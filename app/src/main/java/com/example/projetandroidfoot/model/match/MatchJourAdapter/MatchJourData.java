package com.example.projetandroidfoot.model.match.MatchJourAdapter;

public class MatchJourData {
    private String AwayTeamName;
    private String HomeTeamName;
    private String imageMatchHome;
    private String imageMatchAway;
    private String homeScore;
    private String awayScore;
    private String matchStatus;
    private int liveStatus;
    public MatchJourData(String homeTeamName, String awayTeamName,String homeTeamLogo,String awayTeamLogo,String HomeScore,String AwayScore,String MatchStatus,int LiveStatus){
        this.HomeTeamName=homeTeamName;
        this.AwayTeamName=awayTeamName;
        this.imageMatchHome=homeTeamLogo;
        this.imageMatchAway=awayTeamLogo;
        this.homeScore=HomeScore;
        this.awayScore=AwayScore;
        this.matchStatus=MatchStatus;
        this.liveStatus=LiveStatus;

    }

    public String getHomeTeamName() {
        return HomeTeamName;
    }
    public String getAwayTeamName() {
        return AwayTeamName;
    }
    public String getAwayScore() {
        return awayScore;
    }
    public String getHomeScore() {
        return homeScore;
    }
    public String getImageMatchHome() {
        return imageMatchHome;
    }
    public String getImageMatchAway() {
        return imageMatchAway;
    }
    public int getLiveStatus() {
        return liveStatus;
    }
    public String getMatchStatus() {
        return matchStatus;
    }
}
