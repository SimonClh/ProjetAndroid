package com.example.projetandroidfoot.model.match.MatchJourAdapter;

public class MatchEventsData {
    private String EventName;
    private String EventDetail;
    private String MinuteString;
    private String PlayerString;
    private String AssistString;
    private String EventImage;
    public MatchEventsData(String eventName, String minuteString, String playerString, String assistString, String eventDetail){
        this.EventName=eventName;
        this.MinuteString=minuteString;
        this.PlayerString=playerString;
        this.AssistString=assistString;
        this.EventDetail=eventDetail;
        //this.EventImage=eventImage;
    }
    public String getEventName() {
        return EventName;
    }
    public String getMinuteString() {
        return MinuteString;
    }
    public String getPlayerString() {
        return PlayerString;
    }
    public String getAssistString() {
        return AssistString;
    }

    public String getEventDetail() {
        return EventDetail;
    }

    public String getEventImage() {
        return EventImage;
    }

}
