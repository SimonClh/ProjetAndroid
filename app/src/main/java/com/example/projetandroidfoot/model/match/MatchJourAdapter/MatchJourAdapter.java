package com.example.projetandroidfoot.model.match.MatchJourAdapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projetandroidfoot.R;
import com.squareup.picasso.Picasso;

public class MatchJourAdapter extends RecyclerView.Adapter<MatchJourAdapter.ViewHolder> {

    private MatchJourData[] data;
    private int size=0;
    private OnNoteListener onNoteListener;

    public MatchJourAdapter(MatchJourData[] data, OnNoteListener onNoteListener){
        if(data==null){
            this.size=0;
        }else{
            this.size=data.length;
        }
        this.data=data;
        this.onNoteListener=onNoteListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.match_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem, this.onNoteListener);
        return viewHolder;
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final MatchJourData dataMatch = data[position];
        holder.AwayTeamName.setText(data[position].getAwayTeamName());
        holder.HomeTeamName.setText(data[position].getHomeTeamName());
        holder.homeScore.setText(data[position].getHomeScore());
        holder.awayScore.setText(data[position].getAwayScore());
        holder.matchStatus.setText(data[position].getMatchStatus());
        Picasso.get().load(data[position].getImageMatchHome()).into(holder.imageMatchHome);
        Picasso.get().load(data[position].getImageMatchAway()).into(holder.imageMatchAway);
        if (data[position].getLiveStatus() == 1) {
            holder.homeScore.setTextColor(Color.RED);
            holder.awayScore.setTextColor(Color.RED);
            holder.matchStatus.setTextColor(Color.RED);
        } else{
            holder.homeScore.setTextColor(Color.WHITE);
            holder.awayScore.setTextColor(Color.WHITE);
            holder.matchStatus.setTextColor(Color.WHITE);
        }
    }


    @Override
    public int getItemCount() {
        return this.size;
    }

    public void addAll(MatchJourData[] data){
        this.data=new MatchJourData[data.length];
        for(int i=0;i<data.length;i++){
            this.data[i]=data[i];
        }
        this.size=data.length;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView HomeTeamName;
        public TextView AwayTeamName;
        public TextView homeScore;
        public TextView awayScore;
        public TextView matchStatus;
        public ImageView imageMatchHome;
        public ImageView imageMatchAway;
        public RelativeLayout relativeLayout;
        private OnNoteListener onNoteListener;

        public ViewHolder(View itemView, OnNoteListener onNoteListener) {
            super(itemView);
            this.HomeTeamName = (TextView) itemView.findViewById(R.id.textEvent);
            this.AwayTeamName = (TextView) itemView.findViewById(R.id.textMatchTeamAway);
            this.homeScore = (TextView) itemView.findViewById(R.id.scoreHome);
            this.awayScore = (TextView) itemView.findViewById(R.id.scoreAway);
            this.matchStatus = (TextView) itemView.findViewById(R.id.matchStatus);
            this.imageMatchHome = (ImageView) itemView.findViewById(R.id.eventImage);
            this.imageMatchAway = (ImageView) itemView.findViewById(R.id.imageMatchAway);
            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.relativeLayout);
            itemView.setOnClickListener(this);
            this.onNoteListener=onNoteListener;
        }

        @Override
        public void onClick(View view) {
            this.onNoteListener.onNoteClick(getAdapterPosition());
        }

        public String getHomeTeamName(){
            return ""+this.HomeTeamName;
        }
    }

    public interface OnNoteListener{
        void onNoteClick(int adapterPosition);
    }
}
