package com.example.projetandroidfoot.api;
import com.example.projetandroidfoot.model.match.GetMatchInfo;
import com.example.projetandroidfoot.model.standings.Competition;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIFootballStandings {
    @GET("standings")
    Call<Competition> listRepos(@Header("x-rapidapi-host") String host, @Header("x-rapidapi-key") String key, @Query("season") int seasonyear, @Query("league") String leagueid);

}
