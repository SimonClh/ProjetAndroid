package com.example.projetandroidfoot.api;
import com.example.projetandroidfoot.model.match.GetMatchInfo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIFootballListMatch {
    @GET("fixtures")
    Call<GetMatchInfo> listRepos(@Header("x-rapidapi-host") String host, @Header("x-rapidapi-key") String key, @Query("league") String league, @Query("season") String season, @Query("date") String date);

}
